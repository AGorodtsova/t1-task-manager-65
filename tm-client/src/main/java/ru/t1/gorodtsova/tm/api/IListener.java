package ru.t1.gorodtsova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    void handler(@NotNull ConsoleEvent consoleEvent);

}
