package ru.t1.gorodtsova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.gorodtsova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class Project {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
