package ru.t1.gorodtsova.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/project/create")
    public String create() {
        projectService.create();
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @Nullable final Project project = projectService.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
