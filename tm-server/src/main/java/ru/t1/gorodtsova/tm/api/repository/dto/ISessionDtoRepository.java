package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    @NotNull
    List<SessionDTO> findByUserId(@NotNull String userId);

    @Nullable
    SessionDTO findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
