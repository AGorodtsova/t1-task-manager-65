package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sortColumn")
    List<TaskDTO> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortColumn") String sortColumn);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
