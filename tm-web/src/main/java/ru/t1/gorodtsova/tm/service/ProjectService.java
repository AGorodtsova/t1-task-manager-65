package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.model.Project;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    public void create() {
        @NotNull final Project project = new Project("New project " + System.currentTimeMillis(),
                "New project");
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project) {
        if (project == null) throw new EntityNotFoundException();
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EntityNotFoundException();
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

}
